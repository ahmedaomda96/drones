package com.example.drones;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.example.drones.controllers.DroneController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DronesApplicationTests {

    @Autowired
    DroneController droneController;

    @Test
    void contextLoads() {
        assertNotNull(droneController);
    }

}
