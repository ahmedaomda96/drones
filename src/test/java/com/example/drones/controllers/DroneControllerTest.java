package com.example.drones.controllers;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.drones.mappers.DroneMapper;
import com.example.drones.mappers.MedicationMapper;
import com.example.drones.model.Medication;
import com.example.drones.model.dto.DroneDto;
import com.example.drones.model.dto.MedicationDto;
import com.example.drones.services.DroneService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest(DroneController.class)
class DroneControllerTest {

    @MockBean
    DroneService droneService;
    @MockBean
    DroneMapper droneMapper;
    @MockBean
    MedicationMapper medicationMapper;


    @Autowired
    MockMvc mockMvc;

    @Test
    void testRegisterDrone() throws Exception {
        DroneDto droneDto = createDrone();

        mockMvc.perform(post("/drone/register")
            .content(new ObjectMapper().writeValueAsString(droneDto))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

    }

    @Test
    void testRegisterDrone_invalidDrone() throws Exception {
        DroneDto droneDto = createInvalidDrone();

        mockMvc.perform(post("/drone/register")
            .content(new ObjectMapper().writeValueAsString(droneDto))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());

    }

    @Test
    void testLoadDrone() throws Exception {
        List<MedicationDto> medicationDtoList = createMedicationDtoList();

        mockMvc.perform(post("/drone/{droneId}/load","1")
                .content(new ObjectMapper().writeValueAsString(medicationDtoList))
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

    }

    @Test
    void testLoadDrone_invalidMedication() throws Exception {
        List<MedicationDto> medicationDtoList = createInvalidMedicationDtoList();

        mockMvc.perform(post("/drone/{droneId}/load","1")
                .content(new ObjectMapper().writeValueAsString(medicationDtoList))
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());

    }

    @Test
    void testGetMedication() throws Exception {
        when(droneService.getMedication("1")).thenReturn(createMedicationList());

        mockMvc.perform(get("/drone/{droneId}/medications","1"))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("MED_NAME")));
    }

    @Test
    void testGetReadyDrone() throws Exception {

        mockMvc.perform(get("/drone/ready"))
            .andExpect(status().isOk());
    }

    @Test
    void testGetBatteryLevel() throws Exception {

        when(droneService.getBatteryLevel("1")).thenReturn(25d);

        mockMvc.perform(get("/drone/{droneId}/battery","1"))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("25.0")));
    }


    DroneDto createDrone(){
        DroneDto droneDto = new DroneDto();
        droneDto.setSerialNumber("1");
        droneDto.setModel("middleweight");
        droneDto.setBatteryPercentage(50d);
        droneDto.setWeightLimit(500d);
        droneDto.setState("IDLE");
        return droneDto;
    }

    DroneDto createInvalidDrone(){
        DroneDto droneDto = new DroneDto();
        droneDto.setSerialNumber("1");
        droneDto.setModel("middleweight");
        droneDto.setBatteryPercentage(50d);
        droneDto.setWeightLimit(600d);
        droneDto.setState("IDLE");
        return droneDto;
    }

    List<MedicationDto> createMedicationDtoList(){
        MedicationDto medicationDto = new MedicationDto();
        medicationDto.setName("MED_NAME");
        medicationDto.setCode("CODE_1");
        medicationDto.setWeight(200d);
        medicationDto.setImage("");
        return List.of(medicationDto);
    }

    List<MedicationDto> createInvalidMedicationDtoList(){
        MedicationDto medicationDto = new MedicationDto();
        medicationDto.setName("MED@NAME");
        medicationDto.setCode("CODE_1");
        medicationDto.setWeight(200d);
        medicationDto.setImage("");
        return List.of(medicationDto);
    }

    Set<Medication> createMedicationList(){
        Medication medication = new Medication();
        medication.setName("MED_NAME");
        medication.setCode("CODE_1");
        medication.setWeight(200d);
        medication.setImage("");
        return Set.of(medication);
    }


}
