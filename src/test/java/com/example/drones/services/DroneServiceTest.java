package com.example.drones.services;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import com.example.drones.model.Drone;
import com.example.drones.model.Medication;
import com.example.drones.model.enums.Model;
import com.example.drones.model.enums.State;
import com.example.drones.repository.DroneRepository;
import com.example.drones.repository.EventLogRepository;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.server.ResponseStatusException;

@ExtendWith(MockitoExtension.class)
class DroneServiceTest {

    @InjectMocks
    DroneService droneService;

    @Mock
    DroneRepository droneRepository;

    @Mock
    EventLogRepository eventLogRepository;

    @Test
    void testRegisterDrone(){
        when(droneRepository.save(any())).thenReturn(createDrone());

        droneService.registerDrone(createDrone());

        verify(droneRepository,times(1)).save(any());
    }

    @Test
    void testLoadDrone(){
        when(droneRepository.findById(any())).thenReturn(Optional.of(createDrone()));

        droneService.loadDrone("1",createMedicationList());

        verify(droneRepository,times(2)).save(any());
    }

    @Test
    void testLoadDrone_noDrone(){
        when(droneRepository.findById(any())).thenReturn(Optional.empty());
        List<Medication> medicationList = createMedicationList();
        assertThrows(ResponseStatusException.class,()->droneService.loadDrone("1",medicationList));
        verify(droneRepository,never()).save(any());
    }

    @Test
    void testLoadDrone_lowBattery(){
        when(droneRepository.findById(any())).thenReturn(Optional.of(createDroneLowBattery()));
        ReflectionTestUtils.setField(droneService,"minBattery",25d);
        List<Medication> medicationList = createMedicationList();
        assertThrows(ResponseStatusException.class,()->droneService.loadDrone("1",medicationList));
        verify(droneRepository,never()).save(any());
    }

    @Test
    void testLoadDrone_heavyMedication(){
        when(droneRepository.findById(any())).thenReturn(Optional.of(createDroneLowBattery()));
        ReflectionTestUtils.setField(droneService,"minBattery",25d);
        List<Medication> medicationList = createHeavyMedicationList();
        assertThrows(ResponseStatusException.class,()->droneService.loadDrone("1",medicationList));
        verify(droneRepository,never()).save(any());
    }

    @Test
    void testGetMedication_noDrone(){
        when(droneRepository.findById(any())).thenReturn(Optional.empty());
        assertThrows(ResponseStatusException.class,()->droneService.getMedication("1"));
    }

    @Test
    void testGetBatteryLevel_noDrone(){
        when(droneRepository.findById(any())).thenReturn(Optional.empty());
        assertThrows(ResponseStatusException.class,()->droneService.getBatteryLevel("1"));
    }

    @Test
    void testBatteryAudit(){
        when(droneRepository.findAll()).thenReturn(List.of(createDrone()));
        droneService.batteryAuditSchedule();
        verify(eventLogRepository,times(1)).save(any());
    }

    Drone createDrone(){
        Drone drone = new Drone();
        drone.setSerialNumber("1");
        drone.setModel(Model.HEAVYWEIGHT);
        drone.setBatteryPercentage(50d);
        drone.setMaxWeight(500d);
        drone.setState(State.IDLE);
        return drone;
    }

    Drone createDroneLowBattery(){
        Drone drone = new Drone();
        drone.setSerialNumber("1");
        drone.setModel(Model.HEAVYWEIGHT);
        drone.setBatteryPercentage(20d);
        drone.setMaxWeight(500d);
        drone.setState(State.IDLE);
        return drone;
    }

    List<Medication> createMedicationList(){
        Medication medication = new Medication();
        medication.setName("MED_NAME");
        medication.setCode("CODE_1");
        medication.setWeight(200d);
        medication.setImage("");
        return List.of(medication);
    }

    List<Medication> createHeavyMedicationList(){
        Medication medication1 = new Medication();
        medication1.setName("MED_NAME_1");
        medication1.setCode("CODE_2");
        medication1.setWeight(200d);
        medication1.setImage("");

        Medication medication2 = new Medication();
        medication2.setName("MED_NAME_2");
        medication2.setCode("CODE_2");
        medication2.setWeight(400d);
        medication2.setImage("");

        return List.of(medication1,medication2);
    }

}
