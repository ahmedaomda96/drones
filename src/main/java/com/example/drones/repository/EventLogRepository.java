package com.example.drones.repository;

import com.example.drones.model.EventLog;
import org.springframework.data.repository.CrudRepository;

public interface EventLogRepository extends CrudRepository<EventLog, Long> {

}
