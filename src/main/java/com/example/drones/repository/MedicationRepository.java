package com.example.drones.repository;

import com.example.drones.model.Medication;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

public interface MedicationRepository extends CrudRepository<Medication, UUID> {

}
