package com.example.drones.repository;

import com.example.drones.model.Drone;
import com.example.drones.model.enums.State;
import java.util.List;
import javax.validation.constraints.Size;
import org.springframework.data.repository.CrudRepository;

public interface DroneRepository extends CrudRepository<Drone,String> {

    List<Drone> findAllByStateAndBatteryPercentageGreaterThanEqual(State state, @Size(max = 100) Double batteryPercentage);
}
