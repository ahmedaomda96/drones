package com.example.drones.mappers;

import com.example.drones.model.Medication;
import com.example.drones.model.dto.MedicationDto;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface MedicationMapper {

    @Mapping(target = "id", ignore = true)
    Medication medicationDtoToMedication(MedicationDto medication);

    List<Medication> medicationDtoToMedication(List<MedicationDto> medication);

}
