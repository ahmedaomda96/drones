package com.example.drones.mappers;

import com.example.drones.model.Drone;
import com.example.drones.model.dto.DroneDto;
import com.example.drones.model.enums.Model;
import com.example.drones.model.enums.State;
import java.util.Arrays;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

@Mapper(componentModel = "spring")
public interface DroneMapper {

    @Mapping(target = "medications", ignore = true)
    @Mapping(target = "maxWeight", source = "weightLimit")
    @Mapping(target = "model",source = "model",qualifiedByName = "modelEnumMapping")
    @Mapping(target = "state",source = "state",qualifiedByName = "stateEnumMapping")
    Drone droneDtoToDrone(DroneDto droneDto);

    @Named("modelEnumMapping")
    default Model modelEnumMapping (String model) {
        return Arrays.stream(Model.values()).filter(model1 -> model1.name().equalsIgnoreCase(model)).findFirst()
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,"Invalid model name"));
    }

    @Named("stateEnumMapping")
    default State stateEnumMapping (String state) {
        return Arrays.stream(State.values()).filter(state1 -> state1.name().equalsIgnoreCase(state)).findFirst()
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,"Invalid state name"));
    }
}
