package com.example.drones.controllers;

import com.example.drones.mappers.DroneMapper;
import com.example.drones.mappers.MedicationMapper;
import com.example.drones.model.Drone;
import com.example.drones.model.Medication;
import com.example.drones.model.dto.DroneDto;
import com.example.drones.model.dto.MedicationDto;
import com.example.drones.model.dto.ValidList;
import com.example.drones.services.DroneService;
import java.util.List;
import java.util.Set;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/drone")
public class DroneController {


    private final DroneService droneService;
    private final DroneMapper droneMapper;
    private final MedicationMapper medicationMapper;

    public DroneController(DroneService droneService, DroneMapper droneMapper, MedicationMapper medicationMapper) {
        this.droneService = droneService;
        this.droneMapper = droneMapper;
        this.medicationMapper = medicationMapper;
    }

    @PostMapping("/register")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void registerDrone(@Valid @RequestBody DroneDto droneDto){
        Drone drone = droneMapper.droneDtoToDrone(droneDto);
        droneService.registerDrone(drone);
    }

    @PostMapping("{droneId}/load")
    public void loadDrone(@PathVariable String droneId,@Valid @RequestBody ValidList<MedicationDto> medicationDtoList){
        List<Medication> medicationList = medicationMapper.medicationDtoToMedication(medicationDtoList);
        droneService.loadDrone(droneId,medicationList);
    }

    @GetMapping("{droneId}/medications")
    public Set<Medication> getMedication(@PathVariable String droneId){
        return droneService.getMedication(droneId);
    }

    @GetMapping("ready")
    public List<Drone> getReadyDrones(){
        return droneService.getAvailableDrones();
    }

    @GetMapping("{droneId}/battery")
    public Double getBatteryLevel(@PathVariable String droneId){
        return droneService.getBatteryLevel(droneId);
    }

}
