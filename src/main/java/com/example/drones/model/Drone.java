package com.example.drones.model;

import com.example.drones.model.enums.Model;
import com.example.drones.model.enums.State;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "drone")
public class Drone {

    @Id
    @Max(100)
    private String serialNumber;

    @Enumerated(EnumType.STRING)
    private Model model;

    @Min(0)
    @Max(100)
    private Double batteryPercentage;

    @Min(0)
    @Max(500)
    private Double maxWeight;

    @Enumerated(EnumType.STRING)
    private State state = State.IDLE;

    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name="drone_id", nullable=false)
    private Set<Medication> medications = new HashSet<>();

}
