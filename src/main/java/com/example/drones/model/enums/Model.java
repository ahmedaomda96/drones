package com.example.drones.model.enums;

public enum Model {
    LIGHTWEIGHT, MIDDLEWEIGHT, CRUISERWEIGHT, HEAVYWEIGHT
}
