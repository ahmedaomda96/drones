package com.example.drones.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@Entity
public class EventLog {

    @Id
    @GeneratedValue
    private Long id;

    private String droneId;

    private String state;

    private String batteryLevel;

    public EventLog(String droneId, String state, String batteryLevel) {
        this.droneId = droneId;
        this.state = state;
        this.batteryLevel = batteryLevel;
    }
}
