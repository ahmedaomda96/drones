package com.example.drones.model.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MedicationDto {

    @Pattern(regexp = "^[a-zA-Z0-9-_]+$",message = "Only letters, numbers, '-' and '_' are allowed")
    private String name;

    @NotNull
    private Double weight;

    @Pattern(regexp = "^[A-Z0-9_]+$",message = "Only upper case letters, numbers and '_' are allowed")
    private String code;

    private String image;

}
