package com.example.drones.model.dto;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import lombok.Data;
import lombok.experimental.Delegate;

@Data
public class ValidList<T> implements List<T> {

    @Valid
    @Delegate
    private List<T> list = new ArrayList<>();
}
