package com.example.drones.model.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DroneDto {

    @Size(max = 100)
    @NotBlank
    private String serialNumber;

    @NotNull
    private String model;

    @Min(0)
    @Max(value = 100,message = "Battery Percentage: ${validatedValue} can't be more than 100.0")
    private Double batteryPercentage;

    @Min(1)
    @Max(value = 500,message = "Weight limit: ${validatedValue} can't be more than 500.0")
    private Double weightLimit;

    private String state;

}
