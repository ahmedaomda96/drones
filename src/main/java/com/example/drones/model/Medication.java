package com.example.drones.model;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "medication")
public class Medication {

    @Id
    @GeneratedValue
    private UUID id;

    private String name;

    private Double weight;

    private String code;

    private String image;

}
