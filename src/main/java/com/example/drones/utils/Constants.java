package com.example.drones.utils;

public class Constants {

    public static final String NO_DRONE_FOUND_EXCEPTION_MESSAGE = "No drone found with id: ";
    public static final String LOW_BATTERY_EXCEPTION_MESSAGE = "Low Battery Drone can't be loaded";
    public static final String HIGH_LOAD_EXCEPTION_MESSAGE = "Load weight exceeds the drone limit";

    private Constants(){}
}
