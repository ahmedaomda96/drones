package com.example.drones.services;

import com.example.drones.model.Drone;
import com.example.drones.model.EventLog;
import com.example.drones.model.Medication;
import com.example.drones.model.enums.State;
import com.example.drones.repository.DroneRepository;
import com.example.drones.repository.EventLogRepository;
import com.example.drones.utils.Constants;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class DroneService {

    private final DroneRepository droneRepository;
    private final EventLogRepository eventLogRepository;

    @Value("${drone.battery.minLimit}")
    private double minBattery;

    public DroneService(DroneRepository droneRepository, EventLogRepository eventLogRepository) {
        this.droneRepository = droneRepository;
        this.eventLogRepository = eventLogRepository;
    }

    public void registerDrone(Drone drone){
        droneRepository.save(drone);
    }

    public void loadDrone(String droneId, List<Medication> medicationList){
        Optional<Drone> optionalDrone = droneRepository.findById(droneId);
        if(optionalDrone.isPresent()){
            Drone drone = optionalDrone.get();
            if(canLoadDrone(drone)){
                drone.setState(State.LOADING);
                droneRepository.save(drone);
            }else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,Constants.LOW_BATTERY_EXCEPTION_MESSAGE);
            }
            Double currentDroneWeight = getCurrentDroneWeight(drone);
            if(currentDroneWeight + medicationList.stream().mapToDouble(Medication::getWeight).sum() > drone.getMaxWeight()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,Constants.HIGH_LOAD_EXCEPTION_MESSAGE);
            }else {
                drone.getMedications().addAll(medicationList);
                drone.setState(State.LOADED);
                droneRepository.save(drone);
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        drone.setMedications(new HashSet<>());
                        drone.setState(State.IDLE);
                        drone.setBatteryPercentage(drone.getBatteryPercentage()-5);
                        droneRepository.save(drone);
                    }
                },5000);
            }
        }else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.NO_DRONE_FOUND_EXCEPTION_MESSAGE + droneId);
        }
    }

    private boolean canLoadDrone(Drone drone) {
        return drone.getBatteryPercentage() >= minBattery;
    }

    private Double getCurrentDroneWeight(Drone drone){
        return drone.getMedications().stream().mapToDouble(Medication::getWeight).sum();
    }

    public Set<Medication> getMedication(String droneId){
        Optional<Drone> optionalDrone = droneRepository.findById(droneId);
        return optionalDrone.map(Drone::getMedications)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.NO_DRONE_FOUND_EXCEPTION_MESSAGE + droneId));
    }

    public List<Drone> getAvailableDrones(){
        return droneRepository.findAllByStateAndBatteryPercentageGreaterThanEqual(State.IDLE,minBattery);
    }

    public Double getBatteryLevel(String droneId){
        Optional<Drone> optionalDrone = droneRepository.findById(droneId);
        return optionalDrone.map(Drone::getBatteryPercentage)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.NO_DRONE_FOUND_EXCEPTION_MESSAGE + droneId));

    }

    @Scheduled(fixedDelayString = "${drone.audit.delay}")
    public void batteryAuditSchedule(){
        Iterable<Drone> all = droneRepository.findAll();
        all.forEach(drone -> eventLogRepository.save(new EventLog(drone.getSerialNumber(),drone.getState().toString(),drone.getBatteryPercentage().toString())));
    }

}
