# Drone Control App By Ahmed Emad

A small REST APIs that allows clients to communicate with drones that  deliver small items.

## Requirements

For building and running the application you need:

- [JDK 11](https://www.oracle.com/de/java/technologies/javase/jdk11-archive-downloads.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

You can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
$ mvn spring-boot:run
```
On Windows using terminal on the project directory:
```shell
$ ./mvnw spring-boot:run
```

You can then access the end points at http://localhost:8080/

## Building and testing

To build and test, you can run:  

```shell
$ cd drones
$ ./mvnw clean install
```

## Database

This app is using H2 in memory database accessable at http://localhost:8080/h2-console

JDBC URL: jdbc:h2:mem:dronedb

User Name: sa

with no password

## Features

This app can perform the following:

1.  Register drone
2.  loading a drone with medication items
3.  checking loaded medication items for a given drone
4.  checking available drones for loading
5.  check drone battery level for a given drone

## Register Drone

```
POST /drone/register
Accept: application/json
Content-Type: application/json

{
    "serialNumber": "fpGeSs8o",
    "model": "middleweight",
    "batteryPercentage": "50.0",
    "weightLimit": "500.0",
    "state": "idle"
}

RESPONSE: HTTP 201 (Created)
```

For drone model (Lightweight, Middleweight, Cruiserweight, Heavyweight)

For drone state (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING)

**Note: Case doesn't matter**

## Load Drone

```
POST /drone/{droneId}/load
Accept: application/json
Content-Type: application/json

[
  {
    "name": "Medic_1",
    "weight": "150",
    "code": "ASD_ASD_1",
    "image": "iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="
  }
]

RESPONSE: HTTP 200 (OK)
```
Base 64 encoded image

## Check loaded medication items for a given drone

```
GET /drone/{droneId}/medications

RESPOSE: HTTP 200 
[
  {
    "name": "Medic_1",
    "weight": "150",
    "code": "ASD_ASD_1",
    "image": "iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="
  }
]
```

## Check available drones
```
GET /drone/ready

RESPOSE: HTTP 200 
[
  {
    "serialNumber": "fpGeSs8o",
    "model": "MIDDLEWEIGHT",
    "batteryPercentage": 50.0,
    "maxWeight": 500.0,
    "state": "IDLE",
    "medications": []
  }
]
```
A drone considered ready when it's in ready state and has at least 25% of battery 

## Check drone battery level for a given drone

```
GET /drone/{droneId}/battery

RESPOSE: HTTP 200 
CONTENT: 50.0
```

